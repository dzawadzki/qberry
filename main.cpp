#include <iostream>
#include <thread>

#include "node.hpp"

using namespace std;
int main(int argc, char const *argv[])
{
    if (argc != 2)
    {
        cerr << "USAGE: qBerry <priority>\n";
        return 1;
    }
    int priority = atoi(argv[1]);
    cout << priority << "\n";
    Node node{priority};

    std::thread t1(std::bind(&Node::start_client, &node));
    std::thread t2(std::bind(&Node::start_server, &node));

    t1.join();
    t2.join();
}

