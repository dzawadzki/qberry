#pragma once
#include <vector>
#include <thread>
#include <memory>
#include <functional>

#include <zmq.hpp>
#include "zhelpers.hpp"

#include "proto/message.pb.h"

class Server
{
public:
    Server()
        : m_socket(m_ctx, ZMQ_PUB)
    {

    }

    void start()
    {
        m_socket.connect ("tcp://localhost:5560");
    }

    void send()
    {

        for (int request_nbr = 0; request_nbr != 10; request_nbr++)
        {
            zmq::message_t request (5);
            memcpy (request.data (), "Hello", 5);
            std::cout << "Sending Hello " << request_nbr << "…" << std::endl;
            m_socket.send (request);
            sleep(2);
        }
    }

    void reply()
    {
        std::cout << "send reply\n";
        zmq::message_t reply (5);
        memcpy (reply.data (), "World", 5);
        m_socket.send (reply);
        std::cout << "sent\n";
    }

private:
    zmq::context_t m_ctx;
    zmq::socket_t m_socket;
};

class Client
{
public:
    Client()
        : m_socket(m_ctx, ZMQ_SUB)
    {
        // generate random identity
        char identity[10] = {};
        sprintf(identity, "%04X-%04X", within(0x10000), within(0x10000));
        m_identity = identity;
        std::cout << "idnt: " << m_identity << "\n";
    }

    void start()
    {
        m_socket.connect ("tcp://localhost:5559");
    }

    void listen()
    {

        zmq::message_t reply;
        for (;;)
        {
            std::cout << "waiting for messages\n";
            m_socket.recv (&reply);
            std::cout << "message received\n";
        }
    }

private:
    zmq::context_t m_ctx;
    zmq::socket_t m_socket;
    std::string m_identity;
};

class Node {
public:
    explicit Node(int priority)
        : m_priority(priority)
    {}

    void start_server()
    {
        m_server.start();
        m_server.send();
    }

    void start_client()
    {
        m_client.start();
        m_client.listen();
    }

private:
    Server m_server;
    Client m_client;
    const int m_priority;
};
